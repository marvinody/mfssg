templates/post.tmpl
How to Use

#Getting Started
So you want to try setting up this shitty site generator? It's pretty easy to manage but it probably won't get you too far. Was pretty cool to write though!
##Minimum Example
Hopefully you've installed it correctly, if not, go to [#installing](#installing)
Now you should have the `MFSSG` command available

Set up a structure like this (source in samples/simple)
![Sample directory structure for MFSSG](https://i.heart.lolisports.com/marv/ZmJlO.png)
Now in `sample.tmpl` place the following line by itself
```{{.Content}}```
And in `pages/sample.md`,
```sample.tmpl<br>Hello<br>World<br>How are you?```
Run `MFSSG` and a new directory, `out` should be created. Take a look at the generated HTML file in the browser of your choice and you should see the following
![Simple generated html output](https://i.heart.lolisports.com/marv/YmYyM.png)
Where did the "Hello" go?
Well, the markdown 'spec' for MFSSG is as follows:
The first line is a relative location (from wherever the command is being ran) to the requested template file
The second line is the title of the page
And the rest is the content
We'll access the title in the next part
##Fleshing out
So let's begin my changing our structure a bit. Source is in samples/inter
![Intermediate sample directory structure](https://i.heart.lolisports.com/marv/MTgzZ.png)
I'll go in order from the picture
`pages/index.md`
```templates/index.tmpl<br>MFSSG<br>Index Page```
`pages/posts/samplePost.md`
```templates/post.tmpl<br>Sample Post!<br>Hello world<br>How are you?```
`templates/index.tmpl`
```&lt;html&gt;<br>&lt;body&gt;<br>  &lt;h1&gt;<br>    {{.Page.Title}}<br>  &lt;/h1&gt;<br>  &lt;ul&gt;<br>    {{range $idx, $post := .Posts}}<br>      &lt;li&gt;<br>        &lt;a href='{{$post.URL}}'&gt;{{$post.Title}}&lt;/a&gt;<br>      &lt;/li&gt;<br>    {{ end }}<br>  &lt;/ul&gt;<br>&lt;/body&gt;<br>&lt;/html&gt;<br>```
`templates/post.tmpl`
```&lt;html&gt;<br>&lt;body&gt;<br>  &lt;h1&gt;<br>    {{.Title}}<br>  &lt;/h1&gt;<br>  &lt;div&gt;<br>    {{.Content}}<br>  &lt;/div&gt;<br>&lt;/body&gt;<br>&lt;/html&gt;<br>```
So, let's start with the `index.tmpl` file. You can see it's extremely different from the previous template file. To summarize quickly, it's something akin to: for each post, make a list item with an anchor tag (with a link to the page) and the text being the title of the post. The index page gets treated differently from any other page to allow for listing the posts.
It must be be located exactly in `pages/index.md` for the following to apply: the renderer will give it a struct with 2 fields, Page and Posts. Page is essentially what any other template gets, but posts is a collection of other Pages which are located in the directory `pages/posts/`.
As for what a regular Page has, it contains a Title, a (relative) URL, an Abstract (used mainly for the index page's render), and Content. Content and Abstract are both unescaped HTML so be wary, but this allows more flexibility since I was limited in the line length
The post template should be pretty self-explanatory, it's just putting some html skeleton around what we used before
You might have noticed the lack of Abstract and Content in the index template. Content doesn't really have a use unless you need to put something like an introduction, and we'll see Abstract in the next example
##Final Example
Simply take a look at the `pages` directory of the source and you can see how this is structured!
``````
