package main

import (
	"bufio"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strings"
)

type Page struct {
	Title    string
	URL      string
	Content  template.HTML
	template *template.Template
	Abstract template.HTML
	isPost   bool
	isIndex  bool
}

func main() {
	loc := "./pages"
	files := getFiles(loc)
	posts := make([]Page, 0, len(files))
	for _, file := range files {
		fmt.Println(file)
		page := makeFile(file, loc, posts)
		if page.isPost {
			posts = append(posts, page)
		}
	}
	makeFile(fmt.Sprintf("%s/%s", loc, "index.md"), loc, posts)
	os.MkdirAll("./out/static", os.ModePerm)
	exec.Command("cp", "-rf", "./static", "./out").Run() // ignore errors, should be non issue?
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func makeFile(file, loc string, posts []Page) Page {
	extStripped := file[:strings.LastIndex(file, ".")]
	filename := "index.html"
	path := "out" + extStripped[len(loc):]
	os.MkdirAll(path, os.ModePerm)
	page, err := markdownToHTML(file)
	page.URL = extStripped[len(loc):]
	check(err)
	filepath := fmt.Sprintf("%s/%s", path, filename)
	var data interface{} = page
	if page.isIndex {
		filepath = fmt.Sprintf("%s.%s", path, "html")
		data = struct {
			Page  Page
			Posts []Page
		}{page, posts}
	}
	render(page, filepath, data)
	return page
}

func render(page Page, filepath string, data interface{}) {
	f, err := os.Create(filepath)
	check(err)
	defer f.Close()
	err = page.template.Execute(f, data)
	check(err)
}

func markdownToHTML(filename string) (Page, error) {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	lineCount, ABSTRACT_MAX_LENGTH := 0, 200
	title, html, abstract, templateFilename := "", "", "", ""
	isPost := strings.HasPrefix(filename, "./pages/posts/")
	isIndex := strings.HasPrefix(filename, "./pages/index.md")

	for scanner.Scan() {
		lineCount += 1
		text := scanner.Text()
		if lineCount == 1 {
			templateFilename = text
			continue
		} else if lineCount == 2 {
			title = text
			continue
		}
		if len(text) == 0 {
			continue
		}

		if poundCount := strings.Count(text, "#"); poundCount > 0 {
			prefix := strings.Repeat("#", poundCount)
			if strings.HasPrefix(text, prefix) {
				headerText := text[poundCount:]
				headerText = strings.TrimSpace(headerText)
				html += fmt.Sprintf("<a class='nostyle' href='#%s'><h%d id='%s'>%s</h%d></a>", headerText, poundCount, headerText, headerText, poundCount)
				if len(abstract) < ABSTRACT_MAX_LENGTH {
					abstract += fmt.Sprintf("<span class='post-abstract-header size-%d'>%s</span>", poundCount, headerText)
				}
				continue
			}
		}
		text = searchAndReplaceSpecialMarkdown(text,
			[]string{`\*`, `_`, `-`, `~`, "```", "`"},
			[]string{"b", "u", "i", "s", "pre", "code"})
		text = searchAndReplaceImageLink(text)
		html += fmt.Sprintf("<p>%s</p>", text)
		if len(abstract) < ABSTRACT_MAX_LENGTH {
			abstract += text
		}

	}
	pageTemplate, err := template.ParseFiles(templateFilename)
	if err != nil {
		return Page{}, err
	}

	return Page{title, "", template.HTML(html), pageTemplate, template.HTML(abstract), isPost, isIndex}, nil
}
func searchAndReplaceImageLink(str string) string {
	localImageRe := regexp.MustCompile(`\[\[(.*?)\]\]\((.*?)\)`)
	imageRe := regexp.MustCompile(`!\[(.*?)\]\((.*?)\)`)
	linkRe := regexp.MustCompile(`\[(.*?)\]\((.*?)\)`)
	imageFmt, linkFmt := "<span class='image'><img src='%[2]s' title='%[1]s' /><span class='image-caption'>%[1]s</span></span>", "<a href='%[2]s'>%[1]s</a>"
	str = genericRegexReplace(localImageRe, imageFmt, str)
	return genericRegexReplace(linkRe, linkFmt, genericRegexReplace(imageRe, imageFmt, str))

}
func genericRegexReplace(re *regexp.Regexp, strFmt, input string) string {
	var out string
	lastRight := 0
	for _, entry := range re.FindAllStringSubmatchIndex(input, -1) {
		chunkedStrings := chunkStrings(entry, input)
		leftStr := input[lastRight:entry[0]]
		lastRight = entry[1]
		out += leftStr + fmt.Sprintf(strFmt, chunkedStrings...)
	}
	out += input[lastRight:]
	return out
}

func chunkStrings(slice []int, str string) []interface{} {
	count := (len(slice) - 2) / 2
	out := make([]interface{}, count)
	for idx := 2; idx < len(slice); idx += 2 {
		out[idx/2-1] = str[slice[idx]:slice[idx+1]]
	}
	return out
}
func searchAndReplaceSpecialMarkdown(str string, keywords, replace []string) string {
	for idx, keyword := range keywords {
		reg := regexp.MustCompile(fmt.Sprintf(`(?:^%s(?P<rep>.*?[^\\])%[1]s)|(?:[^\\]%[1]s(?P<rep>.*?[^\\])%[1]s)`, keyword))
		leftIdx := 0
		newString := ""
		for _, entry := range reg.FindAllStringSubmatchIndex(str, -1) {
			leftMatchIdx, rightMatchIdx := 2, 3
			if entry[leftMatchIdx] == -1 && entry[rightMatchIdx] == -1 {
				leftMatchIdx, rightMatchIdx = 4, 5
			}
			newString += str[leftIdx : entry[leftMatchIdx]-len(keyword)]
			newString += fmt.Sprintf("<%s>%s</%s>", replace[idx], str[entry[leftMatchIdx]:entry[rightMatchIdx]], replace[idx])
			leftIdx = entry[rightMatchIdx] + len(keyword)
		}
		newString += str[leftIdx:]
		str = newString
	}
	return str
}

func getFiles(path string) []string {
	scan, err := ioutil.ReadDir(path)
	check(err)
	var outFiles []string
	for _, fileInfo := range scan {
		if fileInfo.IsDir() {
			subPath := fmt.Sprintf("%s/%s", path, fileInfo.Name())
			outFiles = append(outFiles, getFiles(subPath)...)
		} else if strings.HasSuffix(fileInfo.Name(), ".md") {
			outFiles = append(outFiles, fmt.Sprintf("%s/%s", path, fileInfo.Name()))
		}
	}
	sort.Slice(outFiles, func(i, j int) bool {
		a, _ := os.Lstat(outFiles[i])
		b, _ := os.Lstat(outFiles[j])
		return a.ModTime().After(b.ModTime())
	})
	return outFiles
}
